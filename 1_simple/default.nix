{ pkgs ? import <nixpkgs> {} }:
pkgs.dockerTools.buildImage {
  name = "hello";
  config.Cmd = [ "${pkgs.hello}/bin/hello" ];
}

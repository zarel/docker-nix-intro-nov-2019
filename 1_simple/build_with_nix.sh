#!/usr/bin/env bash
set -e
set -x
docker-compose up -d
docker-compose exec builder nix-build
docker-compose exec builder cp -L result hello_nix.tar.gz

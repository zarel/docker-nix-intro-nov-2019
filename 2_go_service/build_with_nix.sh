#!/usr/bin/env bash
set -e
set -x
NIX_COMPOSE="docker-compose -f docker-compose-nix.yml -p builder"
$NIX_COMPOSE up -d
$NIX_COMPOSE exec builder nix-build
$NIX_COMPOSE exec builder cp -L result urlshor_nix.tar.gz
docker load < urlshor_nix.tar.gz

{ buildGoPackage, fetchFromGitHub }:

buildGoPackage rec {
  name = "urlshor-r20191129";
  rev = "926be6377f47e6af8fcf2a21b79f881d00c64810";

  goPackagePath = "github.com/rafa-acioly/urlshor";

  src = fetchFromGitHub {
    inherit rev;
    owner = "rafa-acioly";
    repo = "urlshor";
    sha256 = "1g4yk06n4padns53z8ivpk29f4zrx8hw9awdmcqzymb59658p3ik";
  };
  goDeps = ./deps.nix;
}

# file generated from Gopkg.lock using dep2nix (https://github.com/nixcloud/dep2nix)
[
  {
    goPackagePath  = "github.com/go-redis/redis";
    fetch = {
      type = "git";
      url = "https://github.com/go-redis/redis";
      rev =  "7f69d5e32072d151eb6936cbe2306413e8117488";
      sha256 = "1khwaxbxa8vrq1rmn2443b2bfczycs41bwsqa67qcwr5rna4wa67";
    };
  }
  {
    goPackagePath  = "github.com/gorilla/mux";
    fetch = {
      type = "git";
      url = "https://github.com/gorilla/mux";
      rev =  "00bdffe0f3c77e27d2cf6f5c70232a2d3e4d9c15";
      sha256 = "0vr38zdad3zgg7q7nn24knl8axngj7cgmlwa93m17yhsnlvqi531";
    };
  }
  {
    goPackagePath  = "github.com/lib/pq";
    fetch = {
      type = "git";
      url = "https://github.com/lib/pq";
      rev =  "3427c32cb71afc948325f299f040e53c1dd78979";
      sha256 = "08j1smm6rassdssdks4yh9aspa1dv1g5nvwimmknspvhx8a7waqz";
    };
  }
  {
    goPackagePath  = "github.com/rafa-acioly/urlshor";
    fetch = {
      type = "git";
      url = "https://github.com/rafa-acioly/urlshor";
      rev =  "884471587a6b127937891780a967a86dacf430f5";
      sha256 = "00z57md1a9qwndkj9rbinsdxrg52vqqn8fgp1ba6vpzbicd94ygl";
    };
  }
]
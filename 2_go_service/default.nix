{ pkgs ? import <nixpkgs> {} }:
let urlshor = pkgs.callPackage ./urlshor.nix {};
in
pkgs.dockerTools.buildLayeredImage {
  name = "urlshor";
  tag = "latest";
  config = {
    Cmd = [ "${urlshor.bin}/bin/urlshor" ];
  };
}
